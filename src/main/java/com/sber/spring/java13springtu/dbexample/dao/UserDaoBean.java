package com.sber.spring.java13springtu.dbexample.dao;

import com.sber.spring.java13springtu.dbexample.model.Book;
import com.sber.spring.java13springtu.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDaoBean {

    private final Connection connection;
    private final BookDaoBean bookDaoBean;

    public UserDaoBean(Connection connection, BookDaoBean bookDaoBean) {
        this.connection = connection;
        this.bookDaoBean = bookDaoBean;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement sqlUpdate = connection.prepareStatement(
                "insert into users(user_surname, user_firstname, " +
                        "user_birthday, user_phone, user_email, user_books_borrowed) " +
                        "values (?,?,?,?,?,?)");

        sqlUpdate.setString(1, user.getUserSurname());
        sqlUpdate.setString(2, user.getUserName());
        sqlUpdate.setDate(3, Date.valueOf(user.getUserBirthday()));
        sqlUpdate.setString(4, user.getUserPhone());
        sqlUpdate.setString(5, user.getUserEmail());
        sqlUpdate.setString(6, user.getUserBooksBorrowed());
    }

    public void infoUserBooksByPhone (String userPhone) throws SQLException {
        PreparedStatement sqlQuery1 = connection.prepareStatement(
                "select user_books_borrowed from users where user_phone = ?");
        sqlQuery1.setString(1, userPhone);
        ResultSet resultSet1 = sqlQuery1.executeQuery();
        while (resultSet1.next()) {
            String[] titlesBooks = resultSet1.getString(
                    "user_books_borrowed").split(",");
            for (int i = 0; i < titlesBooks.length; i++) {
                bookDaoBean.findBookByTitle(titlesBooks[i]);
            }
        }
    }
}
