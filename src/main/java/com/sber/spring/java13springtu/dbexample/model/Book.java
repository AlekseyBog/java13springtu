package com.sber.spring.java13springtu.dbexample.model;

import lombok.*;


import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @Setter(AccessLevel.NONE)
    private Integer bookId;
    private String bookTitle;
    private String author;
    private Date dateAdded;
}
