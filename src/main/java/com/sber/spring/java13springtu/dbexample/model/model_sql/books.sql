create table books
(
    id serial primary key,
    title varchar(100) not null,
    author varchar(100) not null,
    date_added timestamp not null default current_date
);