package com.sber.spring.java13springtu.dbexample;

import com.sber.spring.java13springtu.dbexample.dao.BookDaoBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.spring.java13springtu.dbexample.constans.DBCConstants.*;

@Configuration
@ComponentScan
public class MyDBApplicationContext {

    @Bean
    @Scope("singleton")
    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":"
                        + PORT + "/" + DB, USER, PASSWORD);
    }

//    @Bean
//    public BookDaoBean bookDaoBean() throws SQLException {
//        return new BookDaoBean(newConnection());
//    }
}
