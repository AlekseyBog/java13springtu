create table users
(
    id serial primary key,
    user_surname varchar(30) not null,
    user_firstname varchar(30) not null,
    user_birthday date not null,
    user_phone varchar(30) unique,
    user_email varchar(30) unique,
    user_books_borrowed varchar(150)
);