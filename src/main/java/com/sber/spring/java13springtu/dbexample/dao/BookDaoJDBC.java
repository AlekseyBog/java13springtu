package com.sber.spring.java13springtu.dbexample.dao;

import com.sber.spring.java13springtu.dbexample.db.DBApp;
import com.sber.spring.java13springtu.dbexample.model.Book;

import java.sql.*;

public class BookDaoJDBC {

    public Book findBookById (Integer bookId){
        try (Connection connection = DBApp.INSTANCE.getConnection()){
            if (connection != null){
                System.out.println("Ура");
            } else {
                System.out.println("Не подключено к бд");
            }
            PreparedStatement sqlQuery = connection.prepareStatement(
                    "select * from books where id = ?");
            sqlQuery.setInt(1, bookId);
            ResultSet resultSet = sqlQuery.executeQuery();
            while (resultSet.next()){
                Book book = new Book();
                book.setBookTitle(resultSet.getString("title"));
                book.setAuthor(resultSet.getString("author"));
                book.setDateAdded(resultSet.getDate("date_added"));
                System.out.println(book);
                return book;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

//    private Connection newConnection() throws SQLException {
//        return DriverManager.getConnection(
//                "jdbc:postgresql://localhost:5432/local_db",
//                "postgres",
//                "12345"
//                );
//    }
}
