package com.sber.spring.java13springtu.dbexample.constans;

public interface DBCConstants {
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
