package com.sber.spring.java13springtu.dbexample.dao;

import com.sber.spring.java13springtu.dbexample.db.DBApp;
import com.sber.spring.java13springtu.dbexample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDaoBean {

    private final Connection connection;

    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer bookId) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement(
                "select * from books where id = ?");
        sqlQuery.setInt(1, bookId);
        ResultSet resultSet = sqlQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setDateAdded(resultSet.getDate("date_added"));
            System.out.println(book);
        }
        return book;
    }

    public Book findBookByTitle(String titleBook) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement(
                "select * from books where title = ?");
        sqlQuery.setString(1, titleBook);
        ResultSet resultSet = sqlQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setDateAdded(resultSet.getDate("date_added"));
            System.out.println(book);
        }
        return book;
    }
}
