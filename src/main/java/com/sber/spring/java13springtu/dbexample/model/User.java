package com.sber.spring.java13springtu.dbexample.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Setter(AccessLevel.NONE)
    private Integer userId;
    private String userSurname;
    private String userName;
    private LocalDate userBirthday;
    private String userPhone;
    private String userEmail;
    private String userBooksBorrowed;
}
