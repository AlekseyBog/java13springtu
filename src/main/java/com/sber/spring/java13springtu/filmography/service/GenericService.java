package com.sber.spring.java13springtu.filmography.service;

import com.sber.spring.java13springtu.filmography.dto.GenericDTO;
import com.sber.spring.java13springtu.filmography.mapper.GenericMapper;
import com.sber.spring.java13springtu.filmography.model.GenericModel;
import com.sber.spring.java13springtu.filmography.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

/**
 * Абстрактный сервис который хранит в себе реализацию CRUD операций по умолчанию
 * Если реализация отличная от того что представлено в этом классе,
 * то она переопределяется в сервисе для конкретной сущности
 *
 * @param <T> - Сущность с которой мы работаем
 * @param <N> - DTO, которую мы будем отдавать/принимать дальше
 */

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    private final GenericRepository<T> repository;

    private final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдено")));
    }

    public N create(N newObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    public void delete(Long id){
        repository.deleteById(id);
    }
}
