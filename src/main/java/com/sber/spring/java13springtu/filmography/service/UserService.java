package com.sber.spring.java13springtu.filmography.service;

import com.sber.spring.java13springtu.filmography.dto.UserDTO;
import com.sber.spring.java13springtu.filmography.mapper.UserMapper;
import com.sber.spring.java13springtu.filmography.model.User;
import com.sber.spring.java13springtu.filmography.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDTO> {

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper) {
        super(userRepository, userMapper);
    }


}
