package com.sber.spring.java13springtu.filmography.controller;

import com.sber.spring.java13springtu.filmography.dto.UserDTO;
import com.sber.spring.java13springtu.filmography.model.User;
import com.sber.spring.java13springtu.filmography.repository.UserRepository;
import com.sber.spring.java13springtu.filmography.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями фильмотеки")
public class UserController
        extends GenericController<User, UserDTO> {

    public UserController(UserService userService) {
        super(userService);
    }
}
