package com.sber.spring.java13springtu.filmography.service;

import com.sber.spring.java13springtu.filmography.dto.DirectorDTO;
import com.sber.spring.java13springtu.filmography.mapper.DirectorMapper;
import com.sber.spring.java13springtu.filmography.model.Director;
import com.sber.spring.java13springtu.filmography.repository.DirectorRepository;
import org.springframework.stereotype.Service;

@Service
public class DirectorService
        extends GenericService<Director, DirectorDTO> {

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper) {
        super(directorRepository, directorMapper);
    }
}
