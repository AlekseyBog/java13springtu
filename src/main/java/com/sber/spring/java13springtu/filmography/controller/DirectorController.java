package com.sber.spring.java13springtu.filmography.controller;

import com.sber.spring.java13springtu.filmography.dto.DirectorDTO;
import com.sber.spring.java13springtu.filmography.model.Director;
import com.sber.spring.java13springtu.filmography.model.Film;
import com.sber.spring.java13springtu.filmography.repository.DirectorRepository;
import com.sber.spring.java13springtu.filmography.repository.FilmRepository;
import com.sber.spring.java13springtu.filmography.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссёры", description = "Контроллер для работы с режиссёрами фильмов")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;

    public DirectorController(DirectorService directorService,
                              FilmRepository filmRepository,
                              DirectorRepository directorRepository) {
        super(directorService);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
    }

    @Operation(description = "Добавить фильм режиссёру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> addFilm (@RequestParam(value = "filmId") Long filmId,
                                             @RequestParam(value = "directorId") Long directorId){
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Запись с переданным ID не найдена"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Запись с переданным ID не найдена"));
        director.getFilms().add(film);
        return ResponseEntity.status(HttpStatus.OK).body(directorRepository.save(director));
    }
}
