package com.sber.spring.java13springtu.filmography.repository;

import com.sber.spring.java13springtu.filmography.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> {
}
