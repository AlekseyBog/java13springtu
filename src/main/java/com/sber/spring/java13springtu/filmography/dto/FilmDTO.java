package com.sber.spring.java13springtu.filmography.dto;

import com.sber.spring.java13springtu.filmography.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FilmDTO extends GenericDTO{

    private String filmTitle;

    private LocalDate premierYear;

    private String country;

    private Genre genre;

    private Set<Long> directorsIds;
}
