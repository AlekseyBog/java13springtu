package com.sber.spring.java13springtu.filmography.mapper;

import com.sber.spring.java13springtu.filmography.dto.OrderDTO;
import com.sber.spring.java13springtu.filmography.model.Order;
import com.sber.spring.java13springtu.filmography.repository.FilmRepository;
import com.sber.spring.java13springtu.filmography.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO>{

    private final FilmRepository filmRepository;

    private final UserRepository userRepository;

    public OrderMapper(ModelMapper modelMapper,
                       FilmRepository filmRepository,
                       UserRepository userRepository) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter());
/*
Почему то подчеркиват красным 3 строки снизу, так и не смог разобраться.
 */

//        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
//                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
//                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильма не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
