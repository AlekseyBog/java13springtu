package com.sber.spring.java13springtu.filmography.mapper;


import com.sber.spring.java13springtu.filmography.dto.GenericDTO;
import com.sber.spring.java13springtu.filmography.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity (D dto);

    D toDTO (E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}
