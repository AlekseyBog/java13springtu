package com.sber.spring.java13springtu.filmography.repository;

import com.sber.spring.java13springtu.filmography.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User>{
}
