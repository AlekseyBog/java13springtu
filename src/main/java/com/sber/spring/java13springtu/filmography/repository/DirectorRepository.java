package com.sber.spring.java13springtu.filmography.repository;

import com.sber.spring.java13springtu.filmography.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository
        extends GenericRepository<Director> {
}
