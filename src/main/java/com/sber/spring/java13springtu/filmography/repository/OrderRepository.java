package com.sber.spring.java13springtu.filmography.repository;

import com.sber.spring.java13springtu.filmography.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
