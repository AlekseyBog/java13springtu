package com.sber.spring.java13springtu.filmography.mapper;

import com.sber.spring.java13springtu.filmography.dto.FilmDTO;
import com.sber.spring.java13springtu.filmography.model.Film;
import com.sber.spring.java13springtu.filmography.model.GenericModel;
import com.sber.spring.java13springtu.filmography.repository.DirectorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO>{

//    private final ModelMapper modelMapper;
    private final DirectorRepository directorRepository;

    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(modelMapper, Film.class, FilmDTO.class);

//        this.modelMapper = modelMapper;
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    public void setupMapper(){
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getDirectorsIds())){
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsIds())));
        } else {
            destination.setDirectors(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Film film){
        return Objects.isNull(film) || Objects.isNull(film.getDirectors())
                ? Collections.emptySet()
                : film.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
