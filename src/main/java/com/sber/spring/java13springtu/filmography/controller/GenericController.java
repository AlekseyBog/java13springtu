package com.sber.spring.java13springtu.filmography.controller;

import com.sber.spring.java13springtu.filmography.dto.GenericDTO;
import com.sber.spring.java13springtu.filmography.model.Film;
import com.sber.spring.java13springtu.filmography.model.GenericModel;
import com.sber.spring.java13springtu.filmography.repository.GenericRepository;
import com.sber.spring.java13springtu.filmography.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public abstract class GenericController<T extends GenericModel, N extends GenericDTO> {

//    private final GenericRepository<T> genericRepository;

    private final GenericService<T, N> service;

    public GenericController(GenericService<T, N> genericService) {
        this.service = genericService;
    }

    @Operation(description = "Получить запись по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getById(@RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.OK).
                body(service.getOne(id));
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(service.listAll());
    }

    @Operation(description = "Добаваить новую запись", method = "create")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> create(@RequestBody N newEntity){
        newEntity.setCreatedWhen(LocalDateTime.now());
        service.create(newEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(newEntity);
    }

    @Operation(description = "Изменить запись", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> updateFilm(@RequestBody N updatedEntity,
                                           @RequestParam (value = "id") Long id){
        updatedEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(service.update(updatedEntity));
    }

    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void delete(@RequestParam(value = "id") Long id){
        service.delete(id);
    }
}
