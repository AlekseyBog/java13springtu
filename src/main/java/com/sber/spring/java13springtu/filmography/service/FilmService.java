package com.sber.spring.java13springtu.filmography.service;

import com.sber.spring.java13springtu.filmography.dto.FilmDTO;
import com.sber.spring.java13springtu.filmography.mapper.FilmMapper;
import com.sber.spring.java13springtu.filmography.model.Film;
import com.sber.spring.java13springtu.filmography.repository.FilmRepository;
import org.springframework.stereotype.Service;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {

//    private final FilmRepository filmRepository;

    protected FilmService(FilmRepository filmRepository,
                          FilmMapper filmMapper) {
        super(filmRepository, filmMapper);
    }

//    public FilmDTO getOne(final Long id){
//        Film film = filmRepository.findById(id).get()
//        return
//    }
}
