package com.sber.spring.java13springtu.filmography.controller;

import com.sber.spring.java13springtu.filmography.dto.FilmDTO;
import com.sber.spring.java13springtu.filmography.model.Director;
import com.sber.spring.java13springtu.filmography.model.Film;
import com.sber.spring.java13springtu.filmography.repository.DirectorRepository;
import com.sber.spring.java13springtu.filmography.repository.FilmRepository;
import com.sber.spring.java13springtu.filmography.service.DirectorService;
import com.sber.spring.java13springtu.filmography.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {

    //    private final FilmRepository filmRepository;
    private final FilmService filmService;

    private final DirectorService directorService;

    public FilmController(FilmService filmService,
                          DirectorService directorService) {
        super(filmService);
        this.filmService = filmService;
        this.directorService = directorService;
    }
    /*
        ЗАПРОС ИЗ ДЗ переделанный на работу через сервисы.
         */
    @Operation(description = "Добавить режисёра фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(
            @RequestParam(value = "filmId") Long filmId,
            @RequestParam(value = "directorId") Long directorId) {

        FilmDTO filmDTO = filmService.getOne(filmId);
        filmDTO.getDirectorsIds().add(directorService.getOne(directorId).getId());
        return ResponseEntity.status(HttpStatus.OK).body(filmService.update(filmDTO));
    }
}
