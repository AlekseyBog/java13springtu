package com.sber.spring.java13springtu.filmography.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DirectorDTO extends GenericDTO {

    private String directorFio;

    private String position;

    private Set<Long> filmsIds;
}
