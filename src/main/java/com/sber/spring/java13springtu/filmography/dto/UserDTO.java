package com.sber.spring.java13springtu.filmography.dto;

import com.sber.spring.java13springtu.filmography.model.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO{

    private String login;

    private String password;

    private String firstName;

    private String lastName;

    private String middleName;

    private LocalDate birthDate;

    private String phone;

    private String address;

    private String email;

    private RoleDTO roles;

    Set<Long> ordersIds;
}
