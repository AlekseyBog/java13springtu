package com.sber.spring.java13springtu.filmography.controller;

import com.sber.spring.java13springtu.filmography.dto.OrderDTO;
import com.sber.spring.java13springtu.filmography.model.Order;
import com.sber.spring.java13springtu.filmography.repository.GenericRepository;
import com.sber.spring.java13springtu.filmography.repository.OrderRepository;
import com.sber.spring.java13springtu.filmography.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
@Tag(name = "Аренда фильмов",
        description = "Контроллер для работы с арендой фильмов пользователям фильмотеки")
public class OrderController extends GenericController<Order, OrderDTO> {

    private final OrderService orderService;
    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }
    /*
    ЗАПРОС ИЗ ДЗ
     */
    @Override
    @Operation(description = "Взять фильм в аренду", method = "purchase")
    @RequestMapping(value = "/purchase", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(OrderDTO newEntity) {
        return super.create(newEntity);
    }

}
