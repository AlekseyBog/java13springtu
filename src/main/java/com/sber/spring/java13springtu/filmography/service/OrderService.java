package com.sber.spring.java13springtu.filmography.service;

import com.sber.spring.java13springtu.filmography.dto.OrderDTO;
import com.sber.spring.java13springtu.filmography.mapper.OrderMapper;
import com.sber.spring.java13springtu.filmography.model.Order;
import com.sber.spring.java13springtu.filmography.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {

    private final OrderRepository orderRepository;

    public OrderService(OrderMapper mapper, OrderRepository orderRepository) {
        super(orderRepository, mapper);
        this.orderRepository = orderRepository;
    }
}
