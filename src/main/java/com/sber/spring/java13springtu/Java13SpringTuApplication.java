package com.sber.spring.java13springtu;

import com.sber.spring.java13springtu.dbexample.MyDBApplicationContext;
import com.sber.spring.java13springtu.dbexample.dao.BookDaoBean;
import com.sber.spring.java13springtu.dbexample.dao.BookDaoJDBC;
import com.sber.spring.java13springtu.dbexample.dao.UserDaoBean;
import com.sber.spring.java13springtu.dbexample.model.Book;
import com.sber.spring.java13springtu.dbexample.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class Java13SpringTuApplication implements CommandLineRunner {

//    private BookDaoBean bookDaoBean;
//
//    private NamedParameterJdbcTemplate jdbcTemplate;
//
//    private UserDaoBean userDaoBean;
//
//    @Autowired
//    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }
//
//    @Autowired
//    public void setBookDaoBean(BookDaoBean bookDaoBean) {
//        this.bookDaoBean = bookDaoBean;
//    }
//
//    @Autowired
//    public void setUserDaoBean(UserDaoBean userDaoBean) {
//        this.userDaoBean = userDaoBean;
//    }

    public static void main(String[] args) {
        SpringApplication.run(Java13SpringTuApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
